# Sam Modules
[![Source](https://img.shields.io/badge/source-mathew/modules-blue.svg?style=flat-square)](https://github.com/mathew/modules)
[![Latest Stable Version](https://poser.pugx.org/Sam/modules/v/stable?format=flat-square)](https://packagist.org/packages/mathew/modules)
[![License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](https://tldrlegal.com/license/mit-license)
[![Total Downloads](https://img.shields.io/packagist/dt/Sam/modules.svg?style=flat-square)](https://packagist.org/packages/Sam/modules)
[![Travis (.org)](https://img.shields.io/travis/Sam/modules.svg?style=flat-square)](https://travis-ci.org/Sam/modules)

Extract and modularize your code for maintainability. Essentially creates "mini-laravel" structures to organize your application.

## Installation
Simply install the package through Composer. From here the package will automatically register its service provider and `Module` facade.

```
composer require mathew/modules
```

### Config
To publish the config file, run the following:

```
php artisan vendor:publish --provider="Sam\Modules\ModulesServiceProvider" --tag="config"
```

## License
The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
