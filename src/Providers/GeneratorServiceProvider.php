<?php

namespace Sam\Modules\Providers;

use Illuminate\Support\ServiceProvider;

class GeneratorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the provided services.
     */
    public function boot()
    {
        //
    }

    /**
     * Register the provided services.
     */
    public function register()
    {
        $generators = [
            'command.make.module'            => \Sam\Modules\Console\Generators\MakeModuleCommand::class,
            'command.make.module.controller' => \Sam\Modules\Console\Generators\MakeControllerCommand::class,
            'command.make.module.middleware' => \Sam\Modules\Console\Generators\MakeMiddlewareCommand::class,
            'command.make.module.migration'  => \Sam\Modules\Console\Generators\MakeMigrationCommand::class,
            'command.make.module.model'      => \Sam\Modules\Console\Generators\MakeModelCommand::class,
            'command.make.module.policy'     => \Sam\Modules\Console\Generators\MakePolicyCommand::class,
            'command.make.module.provider'   => \Sam\Modules\Console\Generators\MakeProviderCommand::class,
            'command.make.module.request'    => \Sam\Modules\Console\Generators\MakeRequestCommand::class,
            'command.make.module.seeder'     => \Sam\Modules\Console\Generators\MakeSeederCommand::class,
            'command.make.module.test'       => \Sam\Modules\Console\Generators\MakeTestCommand::class,
        ];

        foreach ($generators as $slug => $class) {
            $this->app->singleton($slug, function ($app) use ($slug, $class) {
                return $app[$class];
            });

            $this->commands($slug);
        }
    }
}
